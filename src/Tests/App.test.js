import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import React from 'react';
import App from '../Components/App.js';


describe('App', () => {
  let app;
  beforeEach(() => {
    app = render(<App />);
  })
  it("renders the page header", () => {
    const header = app.getByText(/Brandon's TODO List/i);
    expect(header).toBeInTheDocument();
  })
})