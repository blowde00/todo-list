import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import userEvent from '@testing-library/user-event';
import React from 'react';
import TodoForm from '../Components/TodoForm.js';


describe('TodoForm', () => {
  let todoForm;
  beforeEach(() => {
    todoForm = render(<TodoForm/>);
  })
  test('form contains an input field', () => {
    const input = screen.getByRole('textbox');
    expect(input).toBeTruthy;
  })
  test('form contains a label for the input text element', () => {
    const labelText  = screen.getByLabelText(/Please enter a TODO item:/i);
    expect(labelText).toBeTruthy();
  })
  test('form contains a text field that allows users to enter a new TODO item', () => {
    const textInput = screen.getByRole('textbox');
    userEvent.type(textInput, 'Walk the dog');
    expect(textInput).toHaveValue('Walk the dog');
  })
})