import React from 'react';
import { Routes, Route } from 'react-router-dom';
import App from './Components/App';
import TodoForm from './Components/TodoForm';
import About from './Components/About';

function Router() {
  return (
    <Routes>
      <Route path='/' element={<App/>}>
        <Route path='' element={<TodoForm/>}></Route>
        <Route path='about' element={<About/>}></Route>
      </Route>
    </Routes>
  )
}

export default Router;