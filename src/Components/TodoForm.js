import React, { useReducer, useEffect } from "react";
import TodoItem from "./TodoItem";
import TodoContext from "../Context/TodoContext";

const initialState = {
  todoItem: '',
  todoList: []
}
const url = 'https://dg-collective-api.herokuapp.com/api/todos'

function reducer(state, action) {
  switch (action.type) {
    case 'addItem':
      return { todoList: [...state.todoList, action.todoItem] };
    case 'getInput':
      return { ...state, todoItem: action.input };
    case 'delete':
      const updatedList = state.todoList.filter(todoItem => { return todoItem.id !== action.id })
      return { ...state, todoList: updatedList }
    case 'complete':
      const completedItem = state.todoList.map(todoItem => {
        const todoItemCopy = { ...todoItem }
        if (todoItem.id === action.id) {
          todoItemCopy.complete = !todoItemCopy.complete;
        }
        return todoItemCopy;
      })
      return { ...state, todoList: completedItem }
    case 'edit':
      let editItem = state.todoList.find(todoItem => todoItem.id === action.id)
      editItem.name = action.payload
      return { ...state };
    case 'setAllTasks':
      return { ...state, todoList: action.payload };
    default:
      return;
  }
}

const TodoList = () => {
  const [state, dispatch] = useReducer(reducer, initialState);

  const onInputChange = (e) => {
    dispatch({ type: 'getInput', input: e.target.value })
  }

  useEffect(() => {
    fetch(url).then((response) => {
      return response.json();
    }).then((data) => {
      dispatch({ type: 'setAllTasks', payload: data })
    })
  }, []);

  const addTodoItem = (e) => {
    e.preventDefault();
    if (state.todoItem !== '') {
      const addedTask = {
        name: state.todoItem
      }
      const options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(addedTask)
      };
      fetch(url, options).then((response) => {
          return response.json();
      }).then((data) => {
        const newItem = { "id": data.id, 'name': data.name, "complete": false }
        dispatch({ type: 'addItem', todoItem: newItem })
        dispatch({ type: 'getInput', input: '' })
      })
    }
  }

  return (
    <TodoContext.Provider value={[state, dispatch, url]}>
      <div>
        <form className='todoForm'>
          <label className='todoLabel' htmlFor='enterTodo'>Please enter a TODO item:</label>
          <input type='text' name='enterTodo' id='enterTodo' placeholder='Enter your TODO item...' value={state.todoItem} onChange={onInputChange}></input>
          <button className='todoSubmit' type='submit' value='Submit' id='enterTodoButton' onClick={addTodoItem}>Add Item</button>
        </form>
        <div className={state.todoList.length > 0 ? 'listContainer' : ''}>
          <ul className=''>{state.todoList.map(todoItem => {
            return <TodoItem key={todoItem.id} todoItem={todoItem} />
          })}</ul>
        </div>
      </div>
    </TodoContext.Provider>
  )
}

export default TodoList;