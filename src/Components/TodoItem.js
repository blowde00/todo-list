import React, { useContext } from "react";
import TodoContext from "../Context/TodoContext";

function TodoItem({ todoItem, deleteItem }) {
  const [state, dispatch, url] = useContext(TodoContext);

  const deleteTodo = () => {
    const options = {
      method: 'DELETE'
    }
    fetch(`${url}/${todoItem.id}`, options).then(response => {
      if (response.ok) {
        dispatch({ type: 'delete', id: todoItem.id });
      }
    });
  }

  const completeTodo = () => {
    const todoToComplete = {
      complete: !todoItem.complete,
    }
    const options = {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(todoToComplete)
    };
    fetch(`${url}/${todoItem.id}`, options).then(response => {
      if (response.ok) {
        dispatch({ type: 'complete', id: todoItem.id })
      }
    })
  }

  const editTodo = () => {
    let editText = prompt('Edit your TODO item', todoItem.name)
    if (editText === '' || editText === null) {
      editText = todoItem.name;
    }
    if (todoItem.name !== editText) {
    const todoToEdit = {
      name: editText,
    }
    const options = {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(todoToEdit)
    };
    fetch(`${url}/${todoItem.id}`, options).then(response => {
      if (response.ok) {
        dispatch({ type: 'edit', id: todoItem.id, payload: editText })
      }
    })
    } else {
      dispatch({ type: 'edit', id: todoItem.id, payload: editText })
    }
  }

  return (
    <li className='todoList'>
      <span className='listBox'>
        <span className={todoItem.complete ? 'complete' : ''}>
          {todoItem.name}
        </span>
      </span>
      <button className='completeButton' onClick={completeTodo}>✔</button>
      <button className='editButton' onClick={() => { editTodo() }}>🖊</button>
      <button className='deleteButton' onClick={deleteTodo}>✖</button>
    </li>
  );
}

export default TodoItem;