import React from "react";
import { Link } from 'react-router-dom';



const Heading = () => {
  return (
  <div className='heading'>
    <nav className= 'navLinks'>
    <Link className='navHome' to='/'></Link>
		<Link className='navAbout' to='/about'></Link>
   </nav>
    <h1 className='headingText'>Brandon's TODO List</h1>
  </div>
  )
}

export default Heading;